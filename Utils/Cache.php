<?php

namespace Logio\Utils;

readonly class Cache implements ICache
{

    //CACHE_DIR
    public function __construct(
        private string $cacheDir
    ){}

    public function set(string $key, mixed $value): bool
    {
        //write to file
        $file = $this->cacheDir . '/' . $key;
        $content = json_encode($value);
        file_put_contents($file, $content);

        return true;
    }

    public function get(string $key): mixed
    {
        //read from file
        $file = $this->cacheDir . '/' . $key;
        $content = file_get_contents($file);
        return json_decode($content);
    }
}