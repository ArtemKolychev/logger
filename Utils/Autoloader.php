<?php

namespace Logio\Utils;
use Logio;

class Autoloader
{
    private static array $classes = [];

    static function getCache(): Logio\Utils\ICache
    {
        if (self::$classes[CACHE_SERVICE]) {
            return self::$classes[CACHE_SERVICE];
        }

        self::$classes[CACHE_SERVICE] = (new ${CACHE_SERVICE}(ROOT . '/cache'));


        if (self::$classes[CACHE_SERVICE] instanceof Logio\Utils\ICache) {
            throw new \Exception('Cache service must implement ICache interface');
        }

        return self::$classes[CACHE_SERVICE];
    }

    static function getDB(): Logio\DB\IDriver
    {
        if (self::$classes[DB_SERVICE]) {
            return self::$classes[DB_SERVICE];
        }

        self::$classes[DB_SERVICE] = (new ${DB_SERVICE}());

        if (self::$classes[DB_SERVICE] instanceof Logio\DB\IDriver) {
            throw new \Exception('DB service must implement IDriver interface');
        }

        return self::$classes[DB_SERVICE];
    }
}
