<?php

namespace Logio\Utils;

interface ICache
{
    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public function set(string $key, mixed $value): bool;

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed;

}