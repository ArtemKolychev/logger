<?php

use Logio\Product\ProductController;

require_once 'config.php';
require_once 'Utils/Autoloader.php';

$productController = new ProductController();

function sendJson($data): void
{
    header('Content-Type: application/json');
    echo json_encode($data);

    exit(0);
}

// router logic
/*
 * @route /product
 */
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    sendJson($productController->detail($id));
}
