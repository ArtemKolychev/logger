<?php

namespace Logio\DB;
enum DriverType {
    case Elastic;
    case MySQL;
}

class DB implements IDriver
{
    private $driverType;

    /**
     * @throws \Exception
     */
    public function __construct(
    private readonly IElasticSearchDriver | IMySQLDriver $driver,
) {
    if($driver instanceof IElasticSearchDriver) {
        $this->driverType = DriverType::Elastic;
    } else if($driver instanceof IMySQLDriver) {
        $this->driverType = DriverType::MySQL;
    } else {
        throw new \Exception('Invalid driver');
    }
}

    public function findById($id)
    {
       if($this->driverType === DriverType::Elastic) {
           return $this->driver->findById($id);
       } else if($this->driverType === DriverType::MySQL) {
           return $this->driver->findProduct($id);
       }
    }
}