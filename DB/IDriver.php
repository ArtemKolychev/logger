<?php

namespace Logio\DB;

interface IDriver
{
    /**
     * @param string $id
     * @return array
     */
    public function findById($id);
}