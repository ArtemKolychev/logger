<?php

namespace Logio\Product;

class ProductController
{
    private \Logio\DB\IDriver $db;
    private \Logio\Utils\ICache $cache;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->db = \Logio\Utils\Autoloader::getDB();
        $this->cache = \Logio\Utils\Autoloader::getCache();
    }

    /**
     * @param string $id
     * @return string
     * @throws \Exception
     */
    public function detail(string $id): mixed
    {
        $product = $this->cache->get($id);

        if (!$product) {
            $product = $this->db->findById($id);
            $this->cache->set($id, $product);
        }

        return $product;
    }
}